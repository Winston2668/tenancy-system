package pi.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TenancySystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(TenancySystemApplication.class, args);
	}
}
